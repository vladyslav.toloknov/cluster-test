const nodeConfig = require('config')

const config = nodeConfig.util.toObject()

module.exports = config
