const Router = require('koa-router')

const elasticsearch = require('./elasticsearch')
const sockets = require('./sockets')

const router = new Router()

router.post('/', async (ctx, next) => {
    const { message } = ctx.request.body
    const ip = ctx.request.ip

    sockets.broadcast('message', message)

    await elasticsearch.index({
        index: 'cluster-test',
        type: 'users',
        body: {
            message,
            ip
        }
    })
    ctx.body = {status: 'accepted'}
})

module.exports = router