process.env.NODE_CONFIG_DIR = './lib/config'

const http = require('http')
const os = require('os')
const redisAdapter = require('socket.io-redis')
const config = require('config')

const app = require('../../app')
const sockets = require('../sockets')

const redisConfig = config.redis
const port = 3000

const cluster = require("cluster")
let server

if(cluster.isMaster){
    const numWorkers = os.cpus().length

    console.log('Master cluster setting up ' + numWorkers + ' workers...')

    for(let i = 0; i < numWorkers; i++) {
        cluster.fork()
    }

    cluster.on('online', function(worker) {
        console.log('Worker ' + worker.process.pid + ' is online')
    })

    cluster.on('exit', function(worker, code, signal) {
        console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal)
        console.log('Starting a new worker')
        cluster.fork()
    })
} else {
    server = http.createServer(app.callback())
    const redis = redisAdapter({
        host: redisConfig.host,
        port: redisConfig.port,
        auth_pass: redisConfig.pass
    })

    sockets.connect(server, redis)

    server.listen(port)
    server.on('error', onError)
    server.on('listening', onListening)
}


function onError (error) {
    if (error.syscall !== 'listen') {
        throw error
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges')
            process.exit(1)
            break
        case 'EADDRINUSE':
            console.error(bind + ' is already in use')
            process.exit(1)
            break
        default:
            throw error
    }
}


function onListening () {
    var addr = server.address()
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port
    console.log('Start server. Listening on', bind)
}