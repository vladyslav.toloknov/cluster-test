const socketsIO = require('socket.io')

let sockets

const connect = (server, redis) => {

    const io = socketsIO(server)

    io.origins('http://localhost:3001')
    io.adapter(redis)
    io.on('connection', function (socket) {
        console.log('socket call handled by worker with pid ' + process.pid)
    })
    sockets = io.sockets
}

const broadcast = (event, message) => {
    if (sockets){
        sockets.emit(event, message)
    }
}

module.exports = {
    connect,
    broadcast,
}
