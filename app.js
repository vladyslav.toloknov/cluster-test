const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const cookie = require('koa-cookie')
const session = require('koa-generic-session')
const RedisStore = require('koa-redis')
const myRedisCli = require('redis').createClient(process.env.REDIS_URL)
const cors = require('koa2-cors')

const router = require('./lib/router')

const app = new Koa()

app.use(cors({
  origin: 'http://localhost:3001',
  exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
  maxAge: 5,
  credentials: true,
  allowMethods: ['GET', 'POST', 'DELETE'],
  allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
}))

app.use(bodyParser())

app.use(session({
    store: new RedisStore({client: myRedisCli}),
    secret: 'secret',
    resave: false,
    saveUninitialized: false,
}))

router.use(cookie.default())
app.use(router.routes())
   .use(router.allowedMethods())

module.exports = app